from itertools import product
from random import shuffle


class NetworkStructuresIterator:
    def __init__(self, config):
        layers = config.layers
        max_neurons = config.neurons
        layers_ranges = [range(layer.min, layer.max, layer.step)
                         for layer in layers]
        all_networks = set(product(*layers_ranges))
        networks = set([tuple(filter(lambda a: a != 0, x))
                        for x in all_networks
                        if sum(x) <= max_neurons and
                        len(tuple(filter(lambda a: a != 0, x))) != 0])
        print("generated {_len} networks structures".format(_len=len(networks)))
        self._networks = list(networks)
        shuffle(self._networks)

    def __iter__(self):
        return self._networks.__iter__()
