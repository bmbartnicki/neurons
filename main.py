"""To run program pleas run this command

        $ python main.py

Program support interactive console. Type in program help for see instruction.

To run program you need pyyaml, neupy, numpy and tensorflow.
It is very important to have configured tensorflow for hardware support.

Recommended way to run this script is do it by Anacodna, which has already
installed all of needed packages and configured hardware support.

https://www.anaconda.com/

Author:
    Bartlomiej Bartnicki <bmbartnicki@gmail.com>

"""
print("Wait...")
from views import view_help, view_start, view_show, \
    view_list, view_scenarios, view_continue, view_show_continuously

options = {
    "help": view_help,
    "start": view_start,
    "continue": view_continue,
    "show": view_show,
    "list": view_list,
    "scenarios": view_scenarios,
    "watch": view_show_continuously
}
try:
    from tensorflow import Graph, test
    from tensorflow.python.client import device_lib

    print("device_lib.list_local_devices()")
    print(device_lib.list_local_devices())
    print("test.is_gpu_available()")
    print(test.is_gpu_available())

    print("test.gpu_device_name()")
    print(test.gpu_device_name())

    print("Ready")

    with Graph().as_default() as G:
        print(G)
        while True:
            _input = input(">")
            if _input.split(" ")[0] not in options:
                print("Provide 'help' for help.")
            else:
                options[_input.split(" ")[0]](_input.split(" "))
except Exception as e:
    while True:
        _input = input(">")
        if _input.split(" ")[0] not in options:
            print("Provide 'help' for help.")
        else:
            options[_input.split(" ")[0]](_input.split(" "))
