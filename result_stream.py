from datetime import datetime
from os import mkdir, listdir

from neupy import storage
from yaml import dump


class ResultStream:
    def __init__(self, config, name=None):
        self._name = None
        self._base_path = None
        if name is None:
            self._new_stream(config)
        else:
            self._continued_stream(config, name)

    def _new_stream(self, config):
        self._name = str(datetime.now()). \
            replace("-", "."). \
            replace(" ", "."). \
            replace(":", ".")
        self._base_path = "simulations/{_name}".format(_name=self._name)
        mkdir(self._base_path)
        with open("{_base_path}/_config.yaml".format(_base_path=self._base_path), "w") as file:
            file.write(dump(config))

    def _continued_stream(self, config, name):
        self._base_path = "simulations/{name}".format(name=name)

    def add(self, statistic, network):
        yaml = dump(statistic)
        file_name = self._layers_to_file_name(statistic['layers'])
        with open("{_base_path}/{file_name}".
                          format(file_name=file_name, _base_path=self._base_path), "w") as file:
            file.write(yaml)
#        dump(storage.save_dict(network))

#        storage.save_json(network, "{_base_path}/{file_name}.net".
#                          format(file_name=file_name, _base_path=self._base_path))

    @staticmethod
    def get_streams():
        return listdir("simulations")

    @staticmethod
    def get_last_stream():
        return sorted(ResultStream.get_streams())[-1]

    def _get_reports_files_names(self):
        return listdir(self._base_path)

    def _layers_to_file_name(self, layers):
        return ".".join([str(layer) for layer in layers])

    def __contains__(self, item):
        return self._layers_to_file_name(item) in self._get_reports_files_names()
