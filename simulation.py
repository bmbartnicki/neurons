from timeit import default_timer as timer

from neupy import algorithms
from neupy.layers import Relu, Softmax, Input

from configurator import Configurator
from network_structure_iterator import NetworkStructuresIterator
from result_stream import ResultStream


class Simulation:
    _VERBOSE_TRAINING = False
    _MOMENTUM = 0.99
    _TRAINING_STEP = 0.01

    def __init__(self, config, data_provider, name=None):
        print("### create simulation")
        self._data_provider = data_provider
        self._config = Configurator(config)
        self.results_stream = ResultStream(config, name=name)

    def start(self):
        print("### start simulation")
        for number, network_structure in \
                enumerate(NetworkStructuresIterator(self._config)):
            if network_structure in self.results_stream:
                print("network omitted due to it already done")
                continue
            print("## start simulation for network number {number}: {network_structure}".
                  format(number=number + 1,
                         network_structure=network_structure))
            memento_network = self._prepare_mementum_network(
                self._prepare_network(network_structure)
            )
            statistics = self._train(memento_network)
            network_description = {
                "layers": network_structure,
                "depth": 2 + len(network_structure),
                "neurons": sum(network_structure)
            }
            statistics.update(network_description)
            self.results_stream.add(statistics, memento_network)
        print("### end simulation")

    @staticmethod
    def _prepare_network(network_structure):
        n_s = network_structure
        if len(network_structure) == 1:
            network = Input(784) > Relu(n_s[0]) > Softmax(10)
        elif len(n_s) == 2:
            network = Input(784) > Relu(n_s[0]) > Relu(n_s[1]) > Softmax(10)
        elif len(n_s) == 3:
            network = Input(784) > Relu(n_s[0]) > Relu(n_s[1]) > Relu(n_s[2]) \
                      > Softmax(10)
        elif len(n_s) == 4:
            network = Input(784) > Relu(n_s[0]) > Relu(n_s[1]) > Relu(n_s[2]) \
                      > Relu(n_s[3]) > Softmax(10)
        elif len(n_s) == 5:
            network = Input(784) > Relu(n_s[0]) > Relu(n_s[1]) > Relu(n_s[2]) \
                      > Relu(n_s[3]) > Relu(n_s[4]) > Softmax(10)
        else:
            raise Exception("There are incorrect number of layers {n_s}".format(n_s=n_s))
        return network

    def _prepare_mementum_network(self, network):
        print("prepare Mementum network")
        return algorithms.Momentum(
            network,
            loss='categorical_crossentropy',
            batch_size=128,
            step=self._TRAINING_STEP,
            verbose=self._VERBOSE_TRAINING,
            shuffle_data=True,
            momentum=self._MOMENTUM,
            nesterov=True,
        )

    @staticmethod
    def _compute_real_error(expected, predicted):
        not_eq_counter = 0
        for r1, r2 in zip(expected, predicted):
            if r1.tolist().index(max(r1)) != r2.tolist().index(max(r2)):
                not_eq_counter += 1
        return not_eq_counter / len(expected)

    def _train_one_epoch(self, mementum_network, errors):
        input_train, input_test, output_train, output_tests = \
            self._data_provider.data
        mementum_network.train(input_train, output_train,
                               input_test, output_tests,
                               epochs=1)
        prediction = mementum_network.predict(input_test)
        real_error = self._compute_real_error(output_tests, prediction)
        errors.append(real_error)

    def _train(self, mementum_network):
        errors = []
        start = timer()
        for i in range(self._config.epochs):
            print("train {i} from {epochs} epochs".format(i=i,epochs=self._config.epochs))
            self._train_one_epoch(mementum_network, errors)
        end = timer()
        return {"errors": errors,
                "time": end - start,
                "min_error": min(errors)}
