from os import walk
from random import choice

from PIL import Image
from numpy import array, float32
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import OneHotEncoder


class DataProvider:
    def __init__(self, path_to_data="training_set"):
        print("create data provider for {path_to_data}".format(path_to_data=path_to_data))
        self._data = None
        self._data_files = self._find_all_data_files(path_to_data)

    @staticmethod
    def _find_all_data_files(path_to_data):
        results = []
        for dir_name, subdirList, fileList in walk(path_to_data):
            for fname in fileList:
                results.append(('{dir_name}/{fname}'.format(dir_name=dir_name, fname=fname),
                                dir_name.split("/")[-1]))
        print("find {_len} data files".format(_len=len(results)))
        return results

    @property
    def prepared_data(self):
        _labels, _data = zip(*_RandomDataIterator(self._data_files))
        _data = array(_data).astype(float32) / 255.
        _labels = array(_labels).astype(float32)
        labels, datas = _labels, _data - _data.mean(axis=0)
        encoder = OneHotEncoder(sparse=False, categories='auto')
        labels = encoder.fit_transform(labels.reshape(-1, 1))
        return train_test_split(
            datas.astype(float32),
            labels.astype(float32),
            test_size=(1 / 7.))

    @property
    def data(self):
        if self._data is None:
            self._data = self.prepared_data
        return self._data


class _RandomDataIterator:
    def __init__(self, paths):
        self._paths = paths

    def __iter__(self):
        return self

    def __next__(self):
        if len(self._paths) == 0:
            raise StopIteration()
        _path, _class = choice(self._paths)
        self._paths.remove((_path, _class))
        return self.prepare_data(_path, _class)

    @staticmethod
    def prepare_data(_path, _class):
        img_data = Image.open(_path).getdata()
        data = list(img_data)

        return int(_class), data
