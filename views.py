from time import sleep

from yaml import load

from data_provider import DataProvider
from exceptions import WrongConfiguration
from result_analyzer import ResultsAnalyzer, show_plot_of_min_err
from result_stream import ResultStream
from simulation import Simulation

help_message = """According words of Einstein "A person who never made a mistake, 
never tried anything new. " I create this tool for finding the best
neural network structure for solving problem of recognizing handwritten
numbers. All implementation details and scientific basis dear reader you
could find in code or attached article (article is in Polish).  
If you have any question just write to me.

Code is under MIT licence - you can do whats you want with this code and
article but you have to left information about author. Feel free to
experiment. 

COMMANDS:
list - show list of all made simulations
show <ID|NAME> - show simulations results with provided ID or NAME
watch <ID|NAME> - watch (repeated show) simulations results with provided ID or NAME
scenarios - show available scenarios
start <NAME> - start simulation with provided ID (SC*)
continue <ID|NAME> - continue provide simulation

all scenarios are in scenarios.yaml
all simulation are saved in simulations directory

@author: Bartlomiej Bartnicki <bmbartnicki@gmail.com>
"""


def _list_of_simulations():
    return enumerate(sorted(ResultStream.get_streams()))


def view_help(arguments):
    print(help_message)


def view_start(arguments):
    if len(arguments) < 2:
        print("You should execute 'start NAME' for example 'start SC1'")
        return
    with open("scenarios.yaml") as file:
        scenarios = load(file.read())
        if arguments[1] not in scenarios:
            print("Scenario with provided name not exist.")
            return
    try:
        Simulation(scenarios[arguments[1]], DataProvider()).start()
    except WrongConfiguration as wrong_configuration:
        print(wrong_configuration)


def view_show_continuously(arguments):
    while True:
        view_show(arguments, show_plot=False)
        sleep(30)


def view_show(arguments, show_plot=True):
    if len(arguments) < 2:
        print("You should execute 'show ID' for example 'show 0'")
        return
    dict_with_list_of_simuations = dict(_list_of_simulations())
    try:
        _id = int(arguments[1])
        print(dict_with_list_of_simuations.keys())
        if _id in dict_with_list_of_simuations.keys():
            name = dict_with_list_of_simuations[_id]
        else:
            print("There is no simulation with ID or NAME you provided.")
            return
    except ValueError:
        if arguments[1] in dict_with_list_of_simuations.values():
            name = arguments[1]
        else:
            print("There is no simulation with ID or NAME you provided.")
            return
    r_a = ResultsAnalyzer(data_path="simulations/{name}".format(name=name))
    print(r_a)
    if show_plot:
        show_plot_of_min_err(r_a)


def view_list(arguments):
    print("Simulations:")
    for _id, name in _list_of_simulations():
        print("{_id}:\t{name}".format(_id=id, name=name))


def view_scenarios(arguments):
    with open("scenarios.yaml") as file:
        print(file.read())


def view_continue(arguments):
    if len(arguments) < 2:
        print("You should execute 'show ID' for example 'show 0'")
        return
    dict_with_list_of_simuations = dict(_list_of_simulations())
    try:
        _id = int(arguments[1])
        print(dict_with_list_of_simuations.keys())
        if _id in dict_with_list_of_simuations.keys():
            name = dict_with_list_of_simuations[_id]
        else:
            print("There is no simulation with ID or NAME you provided.")
            return
    except ValueError:
        if arguments[1] in dict_with_list_of_simuations.values():
            name = arguments[1]
        else:
            print("There is no simulation with ID or NAME you provided.")
            return

    with open("simulations/{name}/_config.yaml".format(name=name)) as file:
        scenario = load(file.read())
    try:
        Simulation(scenario, DataProvider(), name=name).start()
    except WrongConfiguration as wrong_configuration:
        print(wrong_configuration)
