from pprint import pprint

from exceptions import WrongConfiguration


class Configurator:
    def __init__(self, config):
        print("parse configuration:")
        pprint(config)
        self._validate_keys(config)
        self._validate_values(config)
        self._neurons = config['neurons']
        self._epochs = config['epochs']
        self._layers = [Layer(_max=layer['max'],
                              _min=layer['min'],
                              _step=layer['step'])
                        for layer in config["layers"]]
        print("configuration is ready")

    @property
    def neurons(self):
        return self._neurons

    @property
    def epochs(self):
        return self._epochs

    @property
    def layers(self):
        return self._layers

    @staticmethod
    def _validate_keys(config):
        print("validating keys")
        if "neurons" not in config:
            raise WrongConfiguration("Scenario should contain neurons field.")
        if "epochs" not in config:
            raise WrongConfiguration("Scenario should contain epochs field.")
        if "layers" not in config:
            raise WrongConfiguration("Scenario should contain layers field.")
        if len(config['layers']) < 1:
            raise WrongConfiguration("Layers should contain 1 to 5 layers.")
        if len(config['layers']) > 5:
            raise WrongConfiguration("Layers should contain 1 to 5 layers.")
        for layer in config["layers"]:
            if "min" not in layer:
                raise WrongConfiguration("Layer F{layer} should contain min field.".format(layer=layer))
            if "max" not in layer:
                raise WrongConfiguration("Layer F{layer} should contain max field.".format(layer=layer))
            if "step" not in layer:
                raise WrongConfiguration("Layer F{layer} should contain step field.".format(layer=layer))

    @staticmethod
    def _validate_values(config):
        print("validating values")
        if not isinstance(config['neurons'], int):
            raise WrongConfiguration("Field neurons should be int.")
        if not isinstance(config['epochs'], int):
            raise WrongConfiguration("Field epochs should be int.")
        for layer in config["layers"]:
            if not isinstance(layer['min'], int):
                raise WrongConfiguration("Field min in {layer} should be int.".format(layer=layer))
            if not isinstance(layer['max'], int):
                raise WrongConfiguration("Field max in {layer} should be int.".format(layer=layer))
            if not isinstance(layer['step'], int):
                raise WrongConfiguration("Field step in {layer} should be int.".format(layer=layer))

    def __str__(self):
        return "[{_neurons}, " \
               "{_epochs}, " \
               "{_layers}]".\
            format(_neurons=self._neurons, _epochs=self._epochs, _layer=str(self._layerss))

    def __repr__(self):
        return str(self)


class Layer:
    def __init__(self, _min, _max, _step):
        self._min = _min
        self._max = _max
        self._step = _step

    @property
    def min(self):
        return self._min

    @property
    def max(self):
        return self._max

    @property
    def step(self):
        return self._step

    def __str__(self):
        return "({min}, {max}, {step})".\
            format(_min=self.min, _max=self.max,_step=self.step)

    def __repr__(self):
        return str(self)
