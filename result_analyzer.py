from os import listdir
from statistics import stdev

from yaml import load

from configurator import Configurator
from network_structure_iterator import NetworkStructuresIterator

NL = "\nł"


# todo: new format of data
class ResultsAnalyzer:
    TIME = 0
    NUMBER_OF_LAYERS = 1
    NUMBER_OF_NEURINS = 2
    ERRORS = 3
    LAYERS = 4
    MINIMAL_ERRORS = 5

    def __init__(self, data_path):
        with open("{data_path}/_config.yaml".format(data_path=data_path)) as file:
            self._config = Configurator(load(file.read()))
        self._data = None
        self._expected_size_of_data = len(list(NetworkStructuresIterator(self._config)))
        self._data_path = data_path
        self.name = "Report"

    @property
    def file_names_with_results(self):
        return [file_name
                for file_name in list(listdir(self._data_path))
                if file_name[0] != "_"]

    @property
    def _raw_data(self):
        return [load(open(F"{self._data_path}/{file_name}").read())
                for file_name in self.file_names_with_results]

    @property
    def data(self):
        if self._data is None:
            self._data = self._raw_data
        return self._data

    def _get_column(self, column_name):
        return [data[column_name] for data in self]

    def avg_time(self):
        return sum([time for time in self._get_column('time') if time <= 600]) / len(self)

    def data_left(self):
        return self._expected_size_of_data - len(self)

    def time_left(self):
        return self.data_left() * self.avg_time()

    def total_time(self):
        return sum(self._get_column('time'))

    def percent_done(self):
        return int((1 - self.data_left() / self._expected_size_of_data) * 100)

    def minial_error(self):
        return min(self._get_column("min_error"))

    def avg_min_error(self):
        return sum(self._get_column("min_error")) / len(self)

    def stddev_min_errors(self):
        return stdev(self._get_column("min_error"))

    def _networks_with_error_less_then(self, value):
        return [data['layers'] for data in self if data['min_error'] < value]

    def _best_netowrks_as_str(self, number_of_best_networks=15):
        best_netowkrs = sorted(self.data, key=lambda data: data['min_error'])[:15]
        return "".join([
            F"  {b_n['layers']}" \
            F"{(30- len(str(b_n['layers'])))*' '}" \
            F"depth: {b_n['depth']}\t" \
            F"number of neurons: {b_n['neurons']}\t" \
            F"minimal error: {b_n['min_error']:,.20f}\n"
            for b_n in best_netowkrs])

    def min_err_layers_dim_statistic(self):
        return [
            (data['depth'], data['neurons'], data['min_error'])
            for data in self
        ]

    def __getitem__(self, item):
        return [data[item] for data in self]

    def __iter__(self):
        return self.data.__iter__()

    def __len__(self):
        return len(self.data)

    def __str__(self):
        return \
            F"### {self.name} {(70-len(self.name))*'#'}\n" \
            F"Total Time:                                         {int(self.total_time()/3600)}h\n" \
            F"Average time of Network Learning:                   {int(self.avg_time())}s\n" \
            F"Time to end simulation:                             {int(self.time_left()//3600)}h\n" \
            F"Progress:                                           {self.percent_done()}%\n" \
            F"Minimal Error:                                      {self.minial_error()}\n" \
            F"Average Minimal Error:                              {self.avg_min_error()}\n" \
            F"Standard Deviation of Minimal Errors:               {self.stddev_min_errors()}\n" \
            F"Num of Networks with Error differs less than 0.1%:  {len(self._networks_with_error_less_then(.1))}\n" \
            F"Num of Networks with Error differs less than 0.05%: {len(self._networks_with_error_less_then(.05))}\n" \
            F"Bests Networks: \n{self._best_netowrks_as_str()}\n" \
            F""


def show_plot_of_min_err(r_a):
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D
    Axes3D
    x, y, z = zip(*r_a.min_err_layers_dim_statistic())
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    ax.scatter(x, y, z, c='r', marker='o')

    ax.set_xlabel('X Layers Size')
    ax.set_ylabel('Y Neurons Count')
    ax.set_zlabel('Z Min Error')

    plt.show()
